#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

void rouge();
void Vert();

void _delay(float seconds){
    long endTime = millis() + seconds * 1000;
    while(millis() < endTime)_loop();
}

void rouge()
{
    Serial.println("Passage au rouge");
    Serial.println("  Eteindre le vert");
    digitalWrite(10,0);
    Serial.println("  Allume le orange");
    digitalWrite(11,1);
    _delay(5);
    Serial.println("  Eteint le orange");
    digitalWrite(11,0);
    Serial.println("  Allume le rouge");
    digitalWrite(12,1);
    _delay(10);
    Serial.println("  Eteint le rouge");
    digitalWrite(12,0);
    Serial.println("Passage au vert");
    digitalWrite(10,1);
}

void Vert()
{
    digitalWrite(10,1);
    digitalWrite(11,0);
    digitalWrite(12,0);
}

void setup(){
    Serial.begin(115200);
    pinMode(10,OUTPUT);
    pinMode(11,OUTPUT);
    pinMode(12,OUTPUT);
    pinMode(7,INPUT);
}

void loop(){
    Vert();
    if(digitalRead(7)){
        rouge();
    }
}
