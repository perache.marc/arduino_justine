#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include "IRremoteNEW.h"

double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
void neuf();
double value;
void Eteindre();
void zero();
void six();
void cinq();
void huit();
void trois();
void quatre();
void sept();
void un();
void deux();
IRrecv irrecv(2);
decode_results results;

void Eteindre()
{
    digitalWrite(4,1);    
    digitalWrite(5,1);    
    digitalWrite(6,1);    
    digitalWrite(7,1);    
    digitalWrite(8,1);    
    digitalWrite(9,1);    
    digitalWrite(10,1);    
    digitalWrite(11,1);    
}

void zero()
{
    if(((value)==(3119867648.00))){
        digitalWrite(4,0);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,0);
        digitalWrite(9,0);
        digitalWrite(10,1);
        digitalWrite(11,0);
    }    
}

void un()
{
    if(((value)==(3778927104.00))){
        digitalWrite(4,1);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,1);
        digitalWrite(8,1);
        digitalWrite(9,1);
        digitalWrite(10,1);
        digitalWrite(11,0);
    }    
}

void deux()
{
    if(((value)==(2908251648.00))){
        digitalWrite(4,0);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,0);
        digitalWrite(9,1);
        digitalWrite(10,0);
        digitalWrite(11,1);
    }    
}

void trois()
{
    if(((value)==(657459648.00))){
        digitalWrite(4,0);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,1);
        digitalWrite(9,1);
        digitalWrite(10,0);
        digitalWrite(11,0);
    }    
}

void quatre()
{
    if(((value)==(4120482560.00))){
        digitalWrite(4,1);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,1);
        digitalWrite(8,1);
        digitalWrite(9,0);
        digitalWrite(10,0);
        digitalWrite(11,0);
    }    
}

void cinq()
{
    if(((value)==(1931099648.00))){
        digitalWrite(4,0);
        digitalWrite(5,1);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,1);
        digitalWrite(9,0);
        digitalWrite(10,0);
        digitalWrite(11,0);
    }    
}

void six()
{
    if(((value)==(742730880.00))){
        digitalWrite(4,0);
        digitalWrite(5,1);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,0);
        digitalWrite(9,0);
        digitalWrite(10,0);
        digitalWrite(11,0);
    }    
}

void sept()
{
    if(((value)==(1167253888.00))){
        digitalWrite(4,0);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,1);
        digitalWrite(8,1);
        digitalWrite(9,1);
        digitalWrite(10,1);
        digitalWrite(11,0);
    }    
}

void huit()
{
    if(((value)==(1747313920.00))){
        digitalWrite(4,0);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,0);
        digitalWrite(9,0);
        digitalWrite(10,0);
        digitalWrite(11,0);
    }    
}

void neuf()
{
    if(((value)==(2340753664.00))){
        digitalWrite(4,0);
        digitalWrite(5,0);
        digitalWrite(6,1);
        digitalWrite(7,0);
        digitalWrite(8,1);
        digitalWrite(9,0);
        digitalWrite(10,0);
        digitalWrite(11,0);
    }    
}

void setup(){
    irrecv.enableIRIn();
    Serial.begin(115200);    
    pinMode(4,OUTPUT);
    pinMode(5,OUTPUT);
    pinMode(6,OUTPUT);
    pinMode(7,OUTPUT);
    pinMode(8,OUTPUT);
    pinMode(9,OUTPUT);
    pinMode(10,OUTPUT);
    pinMode(11,OUTPUT);
    Eteindre();
}

void loop(){    
    if( irrecv.decode(&results)){
         irrecv.resume();
        value =  results.value;
        Serial.println(value);
        Eteindre();
        zero();
        un();
        deux();
        trois();
        quatre();
        cinq();
        six();
        sept();
        huit();
        neuf();
    }    
}

