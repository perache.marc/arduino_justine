#include <OneWire.h>

OneWire  ds(10);  // on pin 10 (a 4.7K resistor is necessary)

float celsius, fahrenheit;
  
void setup(void) {
  Serial.begin(9600);
    pinMode(9,OUTPUT);
    pinMode(8,OUTPUT);
    pinMode(7,OUTPUT);
}


void loop(void) {
  loop_temp();
    if((celsius) < (28)){
        digitalWrite(7,1);
    }else{
        digitalWrite(7,0);
    }
    if(((celsius) > (28)) && ((celsius) < (30))){
        digitalWrite(8,1);
    }else{
        digitalWrite(8,0);
    }
    if((celsius) > (30)){
        digitalWrite(9,1);
    }else{
        digitalWrite(9,0);
    }
}
