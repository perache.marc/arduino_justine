echo "Titre du détail"
read FULL_TITLE
echo "Nom court"
read TITLE

cd details
cat template.tex | sed s,"FULL_TITLE","$FULL_TITLE",g | sed s,"TITLE","$TITLE",g > $TITLE.tex

cp ../template/mblock.png ../figures/${TITLE}_IMAGE.png
