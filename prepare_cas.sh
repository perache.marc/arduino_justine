#!/bin/sh

echo "Nouveau projet Hardware"
echo "Nom du répertoire pour le nouveau projet ?"
read REP
echo "Nom complet du nouveau projet ?"
read NOM

cp -r template $REP
cd $REP
cat ../template/text.tex | sed s,"PROJECT_NAME","$NOM",g | sed s,"template","$REP",g > text.tex
cd ..

echo "Ajout des détails"
echo "Titre du détail"
read FULL_TITLE
echo "Nom court"
read TITLE

cd details
cat template.tex | sed s,"FULL_TITLE","$FULL_TITLE",g | sed s,"TITLE","$TITLE",g > $TITLE.tex

cp ../template/mblock.png ../figures/${TITLE}_IMAGE.png
cd ..

cd $REP

cp text.tex text_orig.tex 
cat text_orig.tex | sed s,"SPEC_HARD","$REP",g > text.tex
