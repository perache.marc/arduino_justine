#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include "SerialMP3Player.h"
#include "TM1637Display.h"
#include "RTC.h"

double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
void test_alarme();
double minutes;
double heures;
double heures_alarme;
double minutes_alarme;
void sonnerie_arret();
double sonnerie;
void change_alarme();
double alarme;
void sonnerie_marche();
void affiche();
void reglage_heure_valeur();
void reglage_minutes_valeur();
void reglage_alarme();
void reglage_heure();
RTC time;
uint8_t getTimeOrDate(int x) {
     uint8_t result;
     switch (x) {
          case 1: result=time.seconds;
          break;
          case 2: result=time.minutes;
          break;
          case 3: result=time.hours;
          break;
          case 4: result=time.Hours;
          break;
          case 5: result=time.midday;
          break;
          case 6: result=time.day;
          break;
          case 7: result=time.weekday;
          break;
          case 8: result=time.month;
          break;
          case 9: result=time.year;
      break;}
     return result;
 }
TM1637Display TM1637reveil(12,13) ;
SerialMP3Player mp3(27,25) ;


void test_alarme()
{
    minutes =  getTimeOrDate(2);
    
    heures =  getTimeOrDate(4);
    
    if((((heures)==(heures_alarme))) && (((minutes)==(minutes_alarme)))){
        sonnerie_marche();
    }
    
}

void sonnerie_arret()
{
    Serial.println("Arret sonnerie");
    
    sonnerie = 0;
    
    mp3.stop(); 
    
}

void change_alarme()
{
    if(((alarme)==(1))){
        alarme = 0;
        Serial.println("Alarme a 0");
    }else{
        alarme = 1;
        Serial.println("Alarme a 1");
    }
    
}

void sonnerie_marche()
{
    if(((alarme)==(1))){
        if(((sonnerie)==(0))){
            Serial.println("Marche sonnerie");
            sonnerie = 1;
            mp3.play(); 
        }
    }
    
}

void affiche()
{
    Serial.println(heures+String(":")+minutes);
    
    test_alarme();
    
    TM1637reveil.affiche(heures,minutes); 
    
}

void reglage_heure_valeur()
{
    if(digitalRead(5)){
        Serial.println("Mise a jour de la valeur de l'heure");
        Serial.println(heures);
        heures += 1;
        if((23) < (heures)){
            heures = 0;
        }
        Serial.println(heures);
    }
    
}

void reglage_minutes_valeur()
{
    if(digitalRead(6)){
        Serial.println("Mise a jour de la valeur des minutes");
        Serial.println(minutes);
        minutes += 1;
        if((59) < (minutes)){
            minutes = 0;
        }
        Serial.println(minutes);
    }
    
}

void reglage_alarme()
{
    Serial.println("reglage alarme");
    
    while(!(!(digitalRead(4))))
    {
        _loop();
        heures = heures_alarme;
        minutes = minutes_alarme;
        reglage_heure_valeur();
        reglage_minutes_valeur();
        heures_alarme = heures;
        minutes_alarme = minutes;
        affiche();
        _delay(0.5);
    }
    
}

void reglage_heure()
{
    Serial.println("reglage heure");
    
    while(!(!(digitalRead(3))))
    {
        _loop();
        reglage_heure_valeur();
        reglage_minutes_valeur();
         time.settime(-1,minutes,heures,-1,-1,-1,-1);
        minutes =  getTimeOrDate(2);
        heures =  getTimeOrDate(4);
        affiche();
        _delay(0.5);
    }
    
}


void setup(){
    Serial.begin(115200);
     delay(300);
     time.begin(1,0,0,0);
    mp3.begin(9600);
    delay(500);
    mp3.sendCommand(CMD_SEL_DEV, 0, 2);
    delay(500);
    alarme = 0;
    sonnerie = 0;
    
    pinMode(5,INPUT);
    pinMode(6,INPUT);
    pinMode(4,INPUT);
    pinMode(3,INPUT);
    pinMode(7,INPUT);
}

void loop(){
    
    Serial.println(String("Boucle principale ")+time.gettime("H:i:s"));
     time.gettime();
    minutes =  getTimeOrDate(2);
    heures =  getTimeOrDate(4);
    affiche();
    if(digitalRead(3)){
        reglage_heure();
    }
    if(digitalRead(4)){
        sonnerie_arret();
        reglage_alarme();
    }
    if(digitalRead(7)){
        change_alarme();
    }
    _delay(0.5);
    
    _loop();
}

void _delay(float seconds){
    long endTime = millis() + seconds * 1000;
    while(millis() < endTime)_loop();
}

void _loop(){
    
}

